const mongoose = require("mongoose");
let Schema = mongoose.Schema;
let ServiceSchema = new Schema({
  // serviceId: {type: Number},
  name: { type: String },
  price: { type: Number },
  description: { type: String },
});
module.exports = mongoose.model("Service", ServiceSchema);
