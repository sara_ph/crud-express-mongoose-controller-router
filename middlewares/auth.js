const jwt = require("jsonwebtoken");

exports.authorize = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, "longer-secret-is-better");
        next();
    } catch (error) {
        res.status(401).json({ message: "Authentication failed!" });
    }
};
exports.permissionCheck=(req, res, next) => {
    let id = req.params.id;
    let token = jwt.verify(req.headers.authorization.split(" ")[1], "longer-secret-is-better");
    if (token.userId != id) {
        return res.status(403).json({status_message: "permission denied!"})
    }
    next();
}