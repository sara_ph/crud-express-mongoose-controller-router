 // ////* Reqires
const express = require("express");
const mongoose = require("mongoose");
const router = require("./api/router.js");
// //////////////////*
// ////* Mongoose Connection
mongoose.connect("mongodb://localhost:27017/test");
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () { // we're connected!
    console.log("hello mongoose");
});
// ///////////////
// ////* Express Connection
const app = express();
const expressPort = 3000;
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded
// //////////////////*
app.use("/", router);
// ////* Express Connection
app.listen(expressPort, () => console.log(`Example app listening at http://localhost:${expressPort}`));
