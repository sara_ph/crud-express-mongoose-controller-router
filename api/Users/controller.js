// ////* Reqires
const bcrypt = require("bcrypt");
const User = require("../../model/Users");
const jwt = require('jsonwebtoken');
// ////////////* Controllers
// ////* Create
exports.createUser = async function (req, res) {
    ////// Duplicate Username
    if ((await User.findOne({username: req.body.username})) !== null) {
        return res.status(406).json({error_code: 406, error_message: "Bad Request: Username Is Duplicate"});
    }
    //// Hash password
    await hashPassword(req.body.password).then((hashed) => {
        req.body.password = hashed;
    }).catch((err) => {
        res.send(err);
    });
    ////// Create new user
    try {
        let user = await User.create(req.body);
        return res.status(201).send(user);
    } catch (ex) {
        ex.message;
    }
}
// ////* Login (Generating JWT)
exports.loginUser = async (req, res) => {
    let user;
    //////  Check the existence of the user
    if (user = await User.findOne({username: req.body.username})) {
        //////  Check password
        if (await checkPassword(req.body.password, user.password)) {
            let jwtToken = jwt.sign({ /// Generate jwt
                userId: user._id
            }, "longer-secret-is-better", {expiresIn: "1h"});
            res.status(200).json({token: jwtToken, expiresIn: 3600, _id: user._id});
        } else {
            res.status(401).json({error_code: 401, error_message: "The password is incorrect!"});
        }
    } else {
        res.status(404).json({error_code: 404, error_message: "Not Found"});
    }

}
// ////* Read
exports.readUsers = async (req, res) => {
    const users = await User.find();
    return res.send(users);
}
// ////* Update
exports.updateUser = async (req, res) => {
    ////// Duplicate Username
    if ((await User.findOne({username: req.body.username})) !== null) {
        return res.status(406).json({error_code: 406, error_message: "Bad Request: Username Is Duplicate"});
    }
    ///// Hashing
    await hashPassword(req.body.password).then((hashed) => {
        req.body.password = hashed;
    }).catch((err) => {
        res.send(err);
    });
    ////// Updating
    if (await User.findByIdAndUpdate(req.params.id, {$set: req.body})) {
        return res.status(200).json({status_code: 200, status_message: "OK"});
    } else {
        return res.status(404).json({error_code: 404, error_message: "Not Found"});
    }
}
// ////* Delete
exports.deleteUser = async function (req, res) {
    let id = req.params.id;
    if (await User.findByIdAndRemove(id)) {
        res.status(200).json({status_code: 200, status_message: "OK"});
    } else {
        res.status(404).json({error_code: 404, error_message: "Not Found"});
    }
}

// //////////* Functions
async function hashPassword(password) {
    const saltRounds = 10;
    const hashedPassword = await new Promise((resolve, reject) => {
        bcrypt.hash(password, saltRounds, function (err, hash) {
            if (err) 
                reject(err);
            


            resolve(hash);
        });
    });

    return hashedPassword;
}
async function checkPassword(password, hashed) {

    return new Promise((resolve) => {
        bcrypt.compare(password, hashed, function (err, res) {
            if (err) 
                resolve(false);
            


            if (res) { // Passwords match
                resolve(true);
            } else { // Passwords don't match
                resolve(false);
            }
        });
    });
}
