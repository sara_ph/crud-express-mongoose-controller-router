var express = require('express');
var router = express.Router();
const {authorize, permissionCheck} = require("../../middlewares/auth");
const {createUser, loginUser, readUsers, updateUser, deleteUser} = require('./controller.js');
// ////////* Routers
// ////* Create
router.post("/", createUser);
// ////* Login
router.post("/login", loginUser);
// ////* Read
router.get("/", authorize, readUsers);
// ////* Update
router.put("/:id", authorize, permissionCheck, updateUser);
// ////* Delete
router.delete("/:id", authorize, permissionCheck, deleteUser);

module.exports = router;


