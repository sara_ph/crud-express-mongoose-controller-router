var express = require('express');
var router = express.Router();
const serviceController = require('./controller.js');
// ////////* Routers
// ////* Create
router.post("/", serviceController.createService);
// ////* Read
router.get("/", serviceController.readServices);
// ////* Update
router.put("/:id", serviceController.updateService);
// ////* Delete
router.delete("/:id", serviceController.deleteService);

module.exports = router;
