const router = require("express").Router();
const userRouters = require("./Users/router.js");
const serviceRouter = require("./Services/router.js");

router.use("/users", userRouters);
router.use("/services", serviceRouter);

module.exports=router;